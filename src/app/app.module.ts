import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserProfileModule } from './components/user-profile/user-profile.module';
import { LoginRegisterModule } from "./components/login-register/login-register.module";

@NgModule({
  declarations: [ 
    AppComponent, 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginRegisterModule,
    UserProfileModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
}) 
export class AppModule { }
