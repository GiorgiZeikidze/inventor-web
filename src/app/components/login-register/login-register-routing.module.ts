import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginRegisterComponent } from './login-register.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ConfirmMobileComponent } from './register/confirm-mobile/confirm-mobile.component';
import { DeliveryAddressComponent } from './register/delivery-address/delivery-address.component';
import { FillPrivateInfoComponent } from './register/fill-private-info/fill-private-info.component';
import { SelectCategoryComponent } from './register/select-category/select-category.component';



const routes: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '', component: LoginRegisterComponent, children:[
    { path: 'login', component: LoginComponent }, 
    { path: 'register', component: RegisterComponent, children:[
       { path: '', redirectTo: 'select-category', pathMatch: 'full'}, 
       { path: 'select-category', component: SelectCategoryComponent }, 
       { path: 'confirm-mobile', component: ConfirmMobileComponent },
       { path: 'delivery-address', component: DeliveryAddressComponent }, 
       { path: 'private-info', component: FillPrivateInfoComponent }
    ] }
  ]}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRegisterRoutingModule { }
