import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRegisterComponent } from './login-register.component';
import { LoginRegisterRoutingModule } from "./login-register-routing.module";
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SelectCategoryComponent } from './register/select-category/select-category.component';
import { FillPrivateInfoComponent } from './register/fill-private-info/fill-private-info.component';
import { DeliveryAddressComponent } from './register/delivery-address/delivery-address.component';
import { ConfirmMobileComponent } from './register/confirm-mobile/confirm-mobile.component';
import { ReactiveFormsModule }   from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { LoginService } from "../../services/login.service";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


@NgModule({
  imports: [
    CommonModule,
    SharedModule, 
    LoginRegisterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  declarations: [
    LoginRegisterComponent,
    LoginComponent,
    RegisterComponent,
    SelectCategoryComponent,
    FillPrivateInfoComponent,
    DeliveryAddressComponent,
    ConfirmMobileComponent
  ],
  providers: [
    LoginService
  ]
})
export class LoginRegisterModule { }
