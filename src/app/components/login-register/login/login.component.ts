import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup; 

  isSubmitted: boolean = false; 
  isInvalid: boolean = false; 
  isLoading: boolean = false; 

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private _authService: LoginService
  ) {

    this.loginForm = this.fb.group({

      username: [ '', Validators.required ],
      password: [ '', Validators.required ],

    })
   }

  ngOnInit(): void {

  }; 

  get f() { return this.loginForm.controls }; 


  routeToRegister() {
    const registerURL = '/main/register'

    return this.router.navigate([registerURL]); 
  }; 


  onSubmit(loginForm) {

    this.isSubmitted = true; 
    this.isInvalid = false; 
    this.isLoading = false; 

    if ( this.loginForm.invalid ) {

      return; 

    } else {
      
      this._authService
          .login(loginForm.value)
          .subscribe( data => {

            this.isLoading = false;
            localStorage.setItem('token', data['accessToken']); 
            localStorage.setItem('username', loginForm.value['username']); 
            this.router.navigate(['/user-profile']); 
            
          },err => {
            console.log(err);
            this.isLoading = false; 
            this.isInvalid = true; 
          
            
          } )
  
       
    };

    
  };


}
