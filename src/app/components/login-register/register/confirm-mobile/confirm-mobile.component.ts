import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-mobile',
  templateUrl: './confirm-mobile.component.html',
  styleUrls: ['./confirm-mobile.component.scss']
})
export class ConfirmMobileComponent implements OnInit {

  @Output() result: EventEmitter<any> = new EventEmitter<any>(); 

  confPhoneForm: FormGroup; 
  isSubmitted: boolean = false;  

  constructor(
    private fb: FormBuilder,
    private router: Router  
  ) { 

    this.confPhoneForm = this.fb.group({
      otp: [ '', Validators.required ],
      code: [ '', Validators.required ],

    })
  }

  ngOnInit(): void {
  }


get f() { return this.confPhoneForm.controls }; 

onSubmit(confPhoneForm) {

  this.isSubmitted = true; 

  if ( this.confPhoneForm.invalid ) {
    return;

  } else {
       console.log(confPhoneForm.value);
       this.result.emit(this.confPhoneForm.value); 
       return this.router.navigate(['/user-profile']);
  }; 

}; 



}
