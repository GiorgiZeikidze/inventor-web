import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delivery-address',
  templateUrl: './delivery-address.component.html',
  styleUrls: ['./delivery-address.component.scss']
})
export class DeliveryAddressComponent implements OnInit {

  @Output() result: EventEmitter<any> = new EventEmitter<any>(); 

  deliveryForm: FormGroup; 
  isSubmitted: boolean = false; 


  constructor(
    private fb: FormBuilder,
    private router: Router
  ) {

    this,this.deliveryForm = this.fb.group({
        city: ['', Validators.required],
        flat: ['', Validators.required],
        street: ['', Validators.required]
    })

   }

  ngOnInit(): void {
  }; 


  get f() { return this.deliveryForm.controls }; 

  onSubmit(deliveryForm) {
    console.log(this.deliveryForm.value);
    this.isSubmitted = true; 
    
    if ( this.deliveryForm.invalid ) {
       return;  
      } else {
        
        this.result.emit(this.deliveryForm.value); 
        return this.router.navigate(['/main/register/confirm-mobile'])
         
    }
  
  }

}
