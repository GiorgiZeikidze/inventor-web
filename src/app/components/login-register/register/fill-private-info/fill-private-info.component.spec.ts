import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillPrivateInfoComponent } from './fill-private-info.component';

describe('FillPrivateInfoComponent', () => {
  let component: FillPrivateInfoComponent;
  let fixture: ComponentFixture<FillPrivateInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillPrivateInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillPrivateInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
