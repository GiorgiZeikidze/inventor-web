import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-fill-private-info',
  templateUrl: './fill-private-info.component.html',
  styleUrls: ['./fill-private-info.component.scss']
})
export class FillPrivateInfoComponent implements OnInit {


  @Output() result: EventEmitter<any> = new EventEmitter<any>(); 

  privateInfoForm: FormGroup; 
  isSubmitted: boolean = false; 


  constructor(
    private fb: FormBuilder,
    private router: Router
  ) {
 

    this,this.privateInfoForm = this.fb.group({
      // code: [ '', Validators.required ],
      fullName: [ '', Validators.required ],
      name: [ '', Validators.required ],
      phone: [ '', Validators.required],
      pid: [ '', Validators.required ]


    })

   }

  ngOnInit(): void {
  }; 



  get f() { return this.privateInfoForm.controls; }; 

  onSubmit(privateInfoForm) { 

    this.isSubmitted = true; 
    console.log(this.privateInfoForm.invalid);
    
    if ( this.privateInfoForm.invalid) {

        return;

    } else {
          console.log(this.privateInfoForm.value);
          this.result.emit(this.privateInfoForm.value); 
          this.router.navigate(['/main/register/delivery-address']);
   
    };
     
  }



}
