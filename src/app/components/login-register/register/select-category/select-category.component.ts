import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select-category',
  templateUrl: './select-category.component.html',
  styleUrls: ['./select-category.component.scss']
})
export class SelectCategoryComponent implements OnInit {


  constructor(
    private router: Router 
  ) { }

  ngOnInit(): void {
  }

  

  backToLogin() { 
    return this.router.navigate(['/main/login']); 
  }; 

  nextStep() {
    return this.router.navigate(['/main/register/private-info']); 
  }
}
