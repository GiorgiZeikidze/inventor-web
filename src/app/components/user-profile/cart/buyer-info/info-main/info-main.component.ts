import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-main',
  templateUrl: './info-main.component.html',
  styleUrls: ['./info-main.component.scss']
})
export class InfoMainComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }


  routeToDeliveryDetails() {
    this.router.navigate(['/user-profile/cart/delivery-address']);
  }
}
