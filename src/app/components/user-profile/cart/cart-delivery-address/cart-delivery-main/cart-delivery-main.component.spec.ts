import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartDeliveryMainComponent } from './cart-delivery-main.component';

describe('CartDeliveryMainComponent', () => {
  let component: CartDeliveryMainComponent;
  let fixture: ComponentFixture<CartDeliveryMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartDeliveryMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartDeliveryMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
