import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart-delivery-main',
  templateUrl: './cart-delivery-main.component.html',
  styleUrls: ['./cart-delivery-main.component.scss']
})
export class CartDeliveryMainComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  routeToPayMethod() {
    return this.router.navigate(['/user-profile/cart/payment-method'])
  }

}
