import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentMethodMainComponent } from './payment-method-main.component';

describe('PaymentMethodMainComponent', () => {
  let component: PaymentMethodMainComponent;
  let fixture: ComponentFixture<PaymentMethodMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentMethodMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentMethodMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
