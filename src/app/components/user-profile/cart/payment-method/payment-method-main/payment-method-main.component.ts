import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-method-main',
  templateUrl: './payment-method-main.component.html',
  styleUrls: ['./payment-method-main.component.scss']
})
export class PaymentMethodMainComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  routeToSuccess() {
    return this.router.navigate(['/user-profile/cart/success']);
  }


}
