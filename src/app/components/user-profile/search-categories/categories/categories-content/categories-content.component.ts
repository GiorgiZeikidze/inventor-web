import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-content',
  templateUrl: './categories-content.component.html',
  styleUrls: ['./categories-content.component.scss']
})
export class CategoriesContentComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  changeRoute() {
    this.router.navigate(['/user-profile/search/details/R15 1860 Cordiant SPORT-2 PS-501']);
  }

}
