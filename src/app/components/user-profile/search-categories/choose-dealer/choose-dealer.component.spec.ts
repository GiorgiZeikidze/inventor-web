import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseDealerComponent } from './choose-dealer.component';

describe('ChooseDealerComponent', () => {
  let component: ChooseDealerComponent;
  let fixture: ComponentFixture<ChooseDealerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseDealerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseDealerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
