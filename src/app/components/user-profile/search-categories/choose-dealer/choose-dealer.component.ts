import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-choose-dealer',
  templateUrl: './choose-dealer.component.html',
  styleUrls: ['./choose-dealer.component.scss']
})
export class ChooseDealerComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }


  routeToDealerPrices() {
    this.router.navigate(['/user-profile/search/dealer-details-prices/1']);
  }


}
