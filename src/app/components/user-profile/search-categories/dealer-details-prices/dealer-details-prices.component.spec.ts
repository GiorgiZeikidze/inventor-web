import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerDetailsPricesComponent } from './dealer-details-prices.component';

describe('DealerDetailsPricesComponent', () => {
  let component: DealerDetailsPricesComponent;
  let fixture: ComponentFixture<DealerDetailsPricesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerDetailsPricesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerDetailsPricesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
