import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dealer-details-prices',
  templateUrl: './dealer-details-prices.component.html',
  styleUrls: ['./dealer-details-prices.component.scss']
})
export class DealerDetailsPricesComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }  

  routeToCart() {

    this.router.navigate(['/user-profile/cart/buyer-info']); 
    
  }

}
