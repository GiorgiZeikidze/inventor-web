import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedItemPricesComponent } from './selected-item-prices.component';

describe('SelectedItemPricesComponent', () => {
  let component: SelectedItemPricesComponent;
  let fixture: ComponentFixture<SelectedItemPricesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedItemPricesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedItemPricesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
