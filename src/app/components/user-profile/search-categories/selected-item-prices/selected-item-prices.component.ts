import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-selected-item-prices',
  templateUrl: './selected-item-prices.component.html',
  styleUrls: ['./selected-item-prices.component.scss']
})
export class SelectedItemPricesComponent implements OnInit {

  productID: string; 

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.productID = this.route.snapshot.params.id; 
  };  

  routeToChooseDealer() {
    this.router.navigate(['/user-profile/search/choose-dealer/1'])
  }

}
