import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from './user-profile.component';
import { DashboardMainComponent } from './dashboard-main/dashboard-main.component';
import { SearchCategoriesComponent } from './search-categories/search-categories.component';
import { CategoriesComponent } from './search-categories/categories/categories.component';
import { CategoryDetailComponent } from './search-categories/category-detail/category-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SelectedItemPricesComponent } from './search-categories/selected-item-prices/selected-item-prices.component';
import { ChooseDealerComponent } from './search-categories/choose-dealer/choose-dealer.component';
import { DealerDetailsPricesComponent } from './search-categories/dealer-details-prices/dealer-details-prices.component';
import { CartComponent } from "./cart/cart.component";
import { BuyerInfoComponent } from './cart/buyer-info/buyer-info.component';
import { CartDeliveryAddressComponent } from './cart/cart-delivery-address/cart-delivery-address.component';
import { PaymentMethodComponent } from './cart/payment-method/payment-method.component';
import { PaymentSuccessComponent } from './cart/payment-success/payment-success.component';


const routes: Routes = [

  { path: '', component: UserProfileComponent, children: [
    { path: '', redirectTo: 'dashboard-main', pathMatch: 'full' }, 
    { path: 'dashboard-main', component: DashboardMainComponent },
    { path: 'search', component: SearchCategoriesComponent, children: [
      { path: 'category/:id', component: CategoriesComponent },
      { path: 'details/:id', component: CategoryDetailComponent },
      { path: 'price-dealers/:id', component: SelectedItemPricesComponent },
      { path: 'choose-dealer/:id', component: ChooseDealerComponent },
      { path:  'dealer-details-prices/:id', component: DealerDetailsPricesComponent }
    ]}, 
    { path: 'cart', component: CartComponent, children: [
      { path: 'buyer-info', component: BuyerInfoComponent },
      { path: 'delivery-address', component: CartDeliveryAddressComponent },
      { path: 'payment-method', component:  PaymentMethodComponent},
      { path: 'success', component: PaymentSuccessComponent },

    ] }  
  ]} 
];


@NgModule({
  imports: [
    SharedModule, 
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UserProfileRoutingModule { }
