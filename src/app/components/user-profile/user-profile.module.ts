import { TokenInterceptorService } from './../../shared/services/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile.component';
import { UserProfileRoutingModule } from "./user-profile-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { DashboardMainComponent } from './dashboard-main/dashboard-main.component';
import { CatalogComponent } from './dashboard-main/catalog/catalog.component';
import { TechnicalServiceComponent } from './dashboard-main/technical-service/technical-service.component';
import { SearchCategoriesComponent } from './search-categories/search-categories.component';
import { CategoriesComponent } from './search-categories/categories/categories.component';
import { CategoryDetailComponent } from './search-categories/category-detail/category-detail.component';
import { LeftFilterComponent } from './search-categories/categories/left-filter/left-filter.component';
import { CategoriesContentComponent } from "./search-categories/categories/categories-content/categories-content.component";
import { SelectedItemPricesComponent } from './search-categories/selected-item-prices/selected-item-prices.component';
import { ChooseDealerComponent } from './search-categories/choose-dealer/choose-dealer.component';
import { DealerDetailsPricesComponent } from './search-categories/dealer-details-prices/dealer-details-prices.component';
import { BuyerInfoComponent } from './cart/buyer-info/buyer-info.component';
import { PaymentMethodComponent } from './cart/payment-method/payment-method.component';
import { PaymentSuccessComponent } from './cart/payment-success/payment-success.component';
import { OrdersComponent } from './orders/orders.component';
import { CartComponent } from "./cart/cart.component";
import { CartDeliveryAddressComponent } from './cart/cart-delivery-address/cart-delivery-address.component';
import { InfoMainComponent } from './cart/buyer-info/info-main/info-main.component';
import { CartDeliveryMainComponent } from "./cart/cart-delivery-address/cart-delivery-main/cart-delivery-main.component";
import { PaymentMethodMainComponent } from './cart/payment-method/payment-method-main/payment-method-main.component';
@NgModule({
  imports: [
    CommonModule,  
    UserProfileRoutingModule,
    SharedModule,
  ],
  declarations: [
    UserProfileComponent,
    DashboardMainComponent,
    CatalogComponent,  
    TechnicalServiceComponent,
    SearchCategoriesComponent,
    CategoriesComponent,   
    CategoryDetailComponent,
    LeftFilterComponent,
    CategoriesContentComponent,
    SelectedItemPricesComponent,
    ChooseDealerComponent,
    DealerDetailsPricesComponent,
    BuyerInfoComponent,
    PaymentMethodComponent,
    PaymentSuccessComponent,
    OrdersComponent, 
    CartComponent, 
    CartDeliveryAddressComponent, 
    InfoMainComponent,
    CartDeliveryMainComponent,
    PaymentMethodMainComponent  
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    

  ]
})
export class UserProfileModule { }
