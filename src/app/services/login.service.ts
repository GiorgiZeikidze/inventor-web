import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  apiUrl: any  = environment.apiUrl; 

  loginUrl: string = 'auth/signin'; 

  constructor(
    private http: HttpClient

  ) { }


  login(loginData: any) {

    return this.http
               .post( this.apiUrl + this.loginUrl, loginData ); 

  };




}
