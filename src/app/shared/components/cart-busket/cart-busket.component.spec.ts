import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartBusketComponent } from './cart-busket.component';

describe('CartBusketComponent', () => {
  let component: CartBusketComponent;
  let fixture: ComponentFixture<CartBusketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartBusketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartBusketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
