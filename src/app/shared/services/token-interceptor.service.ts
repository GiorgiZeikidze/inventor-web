import { UtileService } from './utile.service';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService {

  constructor( 
    private injector: Injector,
    private router: Router
) { }


intercept(req, next) {

  let _utileService = this.injector.get(UtileService)
  let tokenizedReq = req.clone({
    setHeaders:{
      Authorization: _utileService.getToken()
      }
    });

    return next.handle(tokenizedReq); 

};


}
