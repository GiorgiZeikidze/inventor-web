import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtileService {

  constructor() { }

  getToken() {

    return localStorage.getItem('token'); 
  }; 


  getUserName() {

    return localStorage.getItem('username'); 
  }; 

  isLoggedIn() {

    return !!localStorage.getItem('token'); 
  }; 


  
}
