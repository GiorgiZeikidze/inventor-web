import { TokenInterceptorService } from './services/token-interceptor.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtileService } from "./services/utile.service";
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HeaderSearchComponent } from './components/header-search/header-search.component';
import { SearchComponent } from './components/header-search/search/search.component';
import { BusketComponent } from './components/header-search/busket/busket.component';
import { CarBannerComponent } from './components/car-banner/car-banner.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CartBusketComponent } from './components/cart-busket/cart-busket.component';

const COMPONENTS = [
  LoadingSpinnerComponent,  
  PageNotFoundComponent,
  HeaderSearchComponent,
  SearchComponent,
  BusketComponent,
  CarBannerComponent,
  CartBusketComponent,

]; 

@NgModule({
  declarations: [
    COMPONENTS,
    SearchComponent,
    BusketComponent,
    CarBannerComponent
  ],
  imports: [
    CommonModule,
    NgbModule, 
  ],
  providers: [
    UtileService,
    TokenInterceptorService
  ],
  exports: [
    COMPONENTS,
    NgbModule
  ]
})
export class SharedModule { }
